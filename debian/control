Source: clustalx
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Charles Plessy <plessy@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               qtbase5-dev,
               qtbase5-dev-tools,
               qttools5-dev-tools
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/clustalx
Vcs-Git: https://salsa.debian.org/med-team/clustalx.git
Homepage: http://www.clustal.org/clustal2/
Rules-Requires-Root: no

Package: clustalx
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: clustalw,
          texlive-latex-extra,
          boxshade
Description: Multiple alignment of nucleic acid and protein sequences (graphical interface)
 This package offers a GUI interface for the Clustal multiple sequence
 alignment program. It provides an integrated environment for performing
 multiple sequence- and profile-alignments to analyse the results.
 The sequence alignment is displayed in a window on the screen.
 A versatile coloring scheme has been incorporated to highlight conserved
 features in the alignment. For professional presentations, one should
 use the texshade LaTeX package or boxshade.
 .
 The pull-down menus at the top of the window allow you to select all the
 options required for traditional multiple sequence and profile alignment.
 You can cut-and-paste sequences to change the order of the alignment; you can
 select a subset of sequences to be aligned; you can select a sub-range of the
 alignment to be realigned and inserted back into the original alignment.
 .
 An alignment quality analysis can be performed and low-scoring segments or
 exceptional residues can be highlighted.
